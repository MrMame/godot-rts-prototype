extends Node
class_name StateMachine

@export var initial_state_id: String

var _states: Dictionary = Dictionary()
var _current_state_id: String

func _ready():
	var states: Array[Node] = find_children("*State", "", true, false)
	
	for state: State in states:
		_states[state._get_id()] = state
		state.transition_to_state.connect(transition_to_state)
	
	transition_to_state(initial_state_id, null)

func transition_to_state(state_id: String, data):
	if _current_state_id:
		var current_state: State = _states[_current_state_id]
		current_state._deactivate()
	
	_current_state_id = state_id
	var state: State = _states[_current_state_id]
	state._activate(data)

class State extends Node2D:
	
	signal transition_to_state(state_id: String, data)
	
	var _is_active: bool
	
	func _get_id() -> String:
		return ""
	
	func _activate(_data):
		_is_active = true
	
	func _deactivate():
		_is_active = false
	
	func _process(delta: float):
		if _is_active:
			_process_state(delta)

	func _process_state(_delta: float):
		pass
